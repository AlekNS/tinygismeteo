package com.myorg.tgm.app.representation;

import com.myorg.tgm.domain.meteo.City;

import javax.validation.constraints.NotNull;

public class CityDto {

    private Long    id;

    @NotNull
    private String  name;

    public CityDto() {
    }

    public CityDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CityDto(City city) {
        if (city.getId() != null) {
            this.id = city.getId();
        }
        this.name = city.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City asCity() {
        City city = new City();

        if (this.id != null) {
            city.setId(this.id);
        }
        city.setName(this.name);

        return city;
    }
}
