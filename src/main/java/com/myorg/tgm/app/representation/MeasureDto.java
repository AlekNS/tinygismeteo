package com.myorg.tgm.app.representation;

import com.myorg.tgm.domain.meteo.City;
import com.myorg.tgm.domain.meteo.Measure;

import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;

public class MeasureDto {

    public final static String dateFormat = "yyyy-MM-dd";

    private Long            id;

    @NotNull
    private Long            city_id;

    private CityDto         city;

    @NotNull
    private String          measureDate;

    @NotNull
    private Double          temperatureValue;

    public MeasureDto() {
    }

    public MeasureDto(Long id, Long city_id, String measureDate, Double temperatureValue) {
        this.id = id;
        this.city_id = city_id;
        this.measureDate = measureDate;
        this.temperatureValue = temperatureValue;
    }

    public MeasureDto(Measure measure) {
        if (measure.getId() != null) {
            this.id = measure.getId();
        }

        if (measure.getCity() != null) {
            this.city_id = measure.getCity().getId();
            this.city = new CityDto(measure.getCity());
        }

        this.measureDate = new SimpleDateFormat(dateFormat).format(measure.getMeasureDate());
        this.temperatureValue = measure.getTemperatureValue();
    }

    public static String getDateFormat() {
        return dateFormat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCity_id() {
        return city_id;
    }

    public void setCity_id(Long city_id) {
        this.city_id = city_id;
    }

    public CityDto getCity() {
        return city;
    }

    public void setCity(CityDto city) {
        this.city = city;
    }

    public String getMeasureDate() {
        return measureDate;
    }

    public void setMeasureDate(String measureDate) {
        this.measureDate = measureDate;
    }

    public Double getTemperatureValue() {
        return temperatureValue;
    }

    public void setTemperatureValue(Double temperatureValue) {
        this.temperatureValue = temperatureValue;
    }

    public Measure asMeasure() throws Exception {
        Measure measure = new Measure();
        if (this.id != null) {
            measure.setId(this.id);
        }

        if (this.getCity_id() != null) {
            measure.setCity(new City(this.getCity_id(), "?"));
        }

        measure.setMeasureDate(new SimpleDateFormat(dateFormat).parse(this.getMeasureDate()));
        measure.setTemperatureValue(this.getTemperatureValue());

        return measure;
    }
}
