package com.myorg.tgm.app;

import com.myorg.tgm.domain.meteo.ICityRepository;
import com.myorg.tgm.domain.meteo.IMeasureRepository;
import com.myorg.tgm.domain.meteo.Measure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Transactional
@Service
public class MeteoMeasuresService {

    private IMeasureRepository measurementRepository;

    @Autowired
    public MeteoMeasuresService(IMeasureRepository measurementRepository) {
        this.measurementRepository = measurementRepository;
    }

    public Collection<Measure> findAllMeasurements(int pageIndex, int limit) {
        pageIndex = Math.max(Math.min(pageIndex, 10000), 0);
        limit = Math.max(Math.min(limit, 100), 1);
        return measurementRepository.findAll(new PageRequest(pageIndex, limit,
            new Sort(new Sort.Order(Sort.Direction.DESC, "measureDate"), new Sort.Order("city.name"))
        )).getContent();
    }

    public Measure findOneById(Long id) {
        Measure measure = measurementRepository.findById(id);
        if (measure == null) {
            throwNotFound();
        }
        return measure;
    }

    public Measure save(Measure measure) {
        if (measure.getId() != null && measurementRepository.findById(measure.getId()) == null) {
            throwNotFound();
        }
        return measurementRepository.save(measure);
    }

    public Measure deleteById(Long id) {
        Measure measure = measurementRepository.findById(id);
        if (measure == null) {
            throwNotFound();
        }
        measurementRepository.deleteById(id);
        return measure;
    }

    private void throwNotFound() {
        throw new RuntimeException("MeasureDto not found");
    }
}
