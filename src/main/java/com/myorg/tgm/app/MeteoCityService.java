package com.myorg.tgm.app;

import com.myorg.tgm.domain.meteo.City;
import com.myorg.tgm.domain.meteo.ICityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Transactional
@Service
public class MeteoCityService {

    private ICityRepository cityRepository;

    @Autowired
    public MeteoCityService(ICityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public Collection<City> findAllCities() {
        return cityRepository.findAll();
    }
}
