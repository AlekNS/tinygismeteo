package com.myorg.tgm.infrastructure.persistence.meteo;

import com.myorg.tgm.domain.meteo.City;
import com.myorg.tgm.domain.meteo.ICityRepository;
import org.springframework.data.repository.Repository;


@org.springframework.stereotype.Repository
public interface JpaCityRepository extends ICityRepository, Repository<City, Long>  {
}
