package com.myorg.tgm.infrastructure.persistence.meteo;

import com.myorg.tgm.domain.meteo.IMeasureRepository;
import com.myorg.tgm.domain.meteo.Measure;
import org.springframework.data.repository.Repository;


@org.springframework.stereotype.Repository
public interface JpaMeasureRepository extends IMeasureRepository, Repository<Measure, Long>  {
}
