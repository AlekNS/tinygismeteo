package com.myorg.tgm.domain.meteo;

import org.hibernate.annotations.Type;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "measures", uniqueConstraints={@UniqueConstraint(columnNames = { "cityId", "measureDate"})})
public class Measure implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long            id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cityId")
    private City            city;

    @Column(nullable = false)
    @Type(type="date")
    private Date            measureDate;

    @Column(nullable = false)
    private Double          temperatureValue;

    public Measure() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Date getMeasureDate() {
        return measureDate;
    }

    public void setMeasureDate(Date measureDate) {
        this.measureDate = measureDate;
    }

    public Double getTemperatureValue() {
        return temperatureValue;
    }

    public void setTemperatureValue(Double temperatureValue) {
        this.temperatureValue = temperatureValue;
    }

}
