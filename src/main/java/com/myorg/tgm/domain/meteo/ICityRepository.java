package com.myorg.tgm.domain.meteo;

import java.util.Collection;

public interface ICityRepository {

    Collection<City> findAll();

}
