package com.myorg.tgm.domain.meteo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IMeasureRepository {

    Page<Measure> findAll(Pageable pageable);
    Measure findById(Long id);
    void deleteById(Long id);
    Measure save(Measure measure);
}
