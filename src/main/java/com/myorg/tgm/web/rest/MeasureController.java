package com.myorg.tgm.web.rest;

import com.myorg.tgm.app.MeteoMeasuresService;
import com.myorg.tgm.app.representation.MeasureDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/measure")
@CrossOrigin("*")
public class MeasureController {

    private MeteoMeasuresService meteoMeasuresService;

    @Autowired
    public MeasureController(MeteoMeasuresService meteoMeasuresService) {
        this.meteoMeasuresService = meteoMeasuresService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @Transactional(readOnly = true)
    public Collection<MeasureDto> index(@RequestParam(defaultValue = "0") Integer page,
                                        @RequestParam(defaultValue = "10") Integer limit) {
        return meteoMeasuresService.findAllMeasurements(page, limit)
                .stream()
                .map(MeasureDto::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/{measureId}", method = RequestMethod.GET)
    public ResponseEntity<?> show(@PathVariable Long measureId) {
        try {
            return ResponseEntity.ok(new MeasureDto(meteoMeasuresService.findOneById(measureId)));
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> store(@RequestBody MeasureDto measure) {
        try {
            return ResponseEntity.ok(new MeasureDto(meteoMeasuresService.save(measure.asMeasure())));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/{measureId}", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@PathVariable Long measureId, @RequestBody MeasureDto measure) {
        try {
            return ResponseEntity.ok(new MeasureDto(meteoMeasuresService.save(measure.asMeasure())));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }

    @RequestMapping(value = "/{measureId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> remove(@PathVariable Long measureId) {
        try {
            return ResponseEntity.ok(new MeasureDto(meteoMeasuresService.deleteById(measureId)));
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
    }
}
