package com.myorg.tgm.web.rest;

import com.myorg.tgm.app.MeteoCityService;
import com.myorg.tgm.app.representation.CityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/city")
@CrossOrigin("*")
public class CityController {

    private MeteoCityService meteoCityService;

    @Autowired
    public CityController(MeteoCityService meteoCityService) {
        this.meteoCityService = meteoCityService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @Transactional(readOnly = true)
    public Collection<CityDto> index() {
        return meteoCityService.findAllCities()
                .stream()
                .map(CityDto::new)
                .collect(Collectors.toList());
    }

}
