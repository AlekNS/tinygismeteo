Demo Project
============

Very simple meteo editor.
Implemented with using of: *Spring Boot*, *Spring JPA*, *AngularJS*.


Installation
-------------

1. Go to the `frontend` folder and run `yarn install` (`NodeJS` and `yarn` should be installed first)
1. Run `npm run build`
1. Go to the folder with `pom.xml` file and execute `mvn package`


Run
--------

Simple execute as `java -jar tgm-0.0.1-BUILD-SNAPSHOT.jar` and go to the `http://localhost:8080`
