export default function (app) {

    app.service('resources', function ($resource) {
        'ngInject';

        var prefix = '/api';
        if (process.env.NODE_ENV === 'development') {
            prefix = 'http://localhost:8080' + prefix;
        }

        this.city = $resource(`${prefix}/city/:id`, { id: '@id' }, {
            query : {
                method : 'GET',
                isArray: true,
                cache : true
            }
        });
        this.measure = $resource(`${prefix}/measure/:id`, { id: '@id' });
    });

    app.service('measureService', function (modalService, resources) {
        'ngInject';

        this.showEditor = function (measure) {
            return modalService.show('/components/modalMeasure.html', {
                controller: function ($scope, $modal) {
                    'ngInject';

                    $scope.cities = resources.city.query();
                    $scope.measure = angular.copy(measure);
                    $scope.measure.measureDate =
                        new Date($scope.measure.measureDate);

                    $scope.onSubmit = function (measure, form) {
                        if (form.$valid) {
                            $modal.hide($scope.measure);
                        }
                    };
                    $scope.onClickCancel = function () {
                        $modal.close();
                    };
                }
            });
        }
    })
}