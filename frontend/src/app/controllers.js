export default function (app) {

    app.controller('HomeCtrl', function ($scope, resources, measureService) {
        'ngInject';

        function updateMeasureList() {
            $scope.measures = resources.measure.query({ limit: 100 });
        }
        updateMeasureList();

        $scope.measureEditor = function (measure) {
            return measureService.showEditor(measure).then(function (measure) {
                measure.city_id = measure.city.id;
                measure.measureDate = measure.measureDate.toISOString().substring(0, 10);
                resources.measure[measure.id ? 'update' : 'save'](measure).$promise
                    .then(updateMeasureList, function () {
                        alert('This measure already exists');
                        return $scope.measureEditor(measure);
                    });
            }, () => {});
        };

        $scope.onDeleteMeasure = function (measure) {
            if (confirm('Delete measurement?')) {
                resources.measure.delete({id:measure.id}).$promise
                    .then(updateMeasureList);
            }
        };
    })
}