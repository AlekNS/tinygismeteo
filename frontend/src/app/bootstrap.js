import angular from 'angular';
import angularResource from 'angular-resource';
import angularRoute from 'angular-route';
import angularMessages from 'angular-messages';
import jQuery from 'jquery';
import lodash from 'lodash';

window.angular = angular;
window.$ = jQuery;
window._ = lodash;

//import 'bootstrap-loader';
import '../style/app.scss';
