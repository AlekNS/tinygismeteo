import './bootstrap';

import appConfig from './config';
import routeConfig from './routes';

const app = angular.module('app', ['ng', 'ngRoute', 'ngMessages', 'ngResource'])
    .config(appConfig)
    .config(routeConfig)
;

import servicesRegistration from './services';
import componentsRegistration from './components/components';
import controllersRegistration from './controllers';

servicesRegistration(app);
componentsRegistration(app);
controllersRegistration(app);
