export default function ($httpProvider, $logProvider, $resourceProvider, $provide) {
    "ngInject";

    $logProvider.debugEnabled(true);

    $provide.factory('defaultHttpInterceptor', function ($q, $rootScope) {
        'ngInject';

        return {
            request: function (config) {
                if (config.url.startsWith('/api/resources/')) {
                    $rootScope.$broadcast('$httpRequest:api');
                }
                return config;
            },
            response: function (response) {
                if (response.config.url.startsWith('/api/resources/')) {
                    $rootScope.$broadcast('$httpResponse:api');
                }
                return response;
            },
            requestError: function (rejection) {
                $rootScope.$broadcast('$httpRequestError', rejection);
                return $q.reject(rejection);
            },
            responseError: function (rejection) {
                $rootScope.$broadcast('$httpResponseError', rejection);
                return $q.reject(rejection);
            }
        };
    });

    $httpProvider.interceptors.push('defaultHttpInterceptor');

    angular.extend($resourceProvider.defaults.actions, {
        update: {method: 'PUT'}
    });

}