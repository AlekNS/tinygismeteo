export default function ($routeProvider, $httpProvider, $logProvider, $resourceProvider, $provide) {
    "ngInject";

    $routeProvider
        .when('/', {
            controller: 'HomeCtrl',
            templateUrl: 'views/home.html'
        })
        .otherwise('/');

}