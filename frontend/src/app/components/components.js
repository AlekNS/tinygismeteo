import modalRegistration from './modal/modal';

export default function (app) {

    app.component('measureTable', {
        templateUrl: '/components/measureTable.html',
        bindings: {
            'measures': '<',
            'onEditMeasure': '&',
            'onDeleteMeasure': '&',
        }
    });

    app.component('measureEditor', {
        templateUrl: '/components/measureEditor.html',
        bindings: {
            'cities': '<',
            'measure': '=',
            'onSubmit': '&',
            'onCancel': '&'
        }
    });

    modalRegistration(app);

}