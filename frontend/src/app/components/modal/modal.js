export default function (app) {

    app.service('modalService', function ($q, $http, $templateCache, $injector, $compile, $window, $rootScope) {

        class Modal {
            constructor(templateUrl, config) {
                this.templateUrl = templateUrl;
                this.backdrop = config.backdrop || true;
                this.controller = config.controller || (() => { });
                this.scope = config.scope;
                this.$element = null;
                this.defer = $q.defer();
            }

            getTemplateContent(templateUrl) {
                return $q((resolve, reject) => {
                    var template;
                    if (!(template = $templateCache.get(templateUrl))) {
                        $http.get(templateUrl).then(function (response) {
                            resolve(response.data);
                        });
                    } else {
                        resolve(template);
                    }
                });
            }

            show() {
                this.getTemplateContent(this.templateUrl).then((template) => {
                    this.scope = $rootScope.$new(true, this.scope);
                    if (this.controller) {
                        var thisController = this.controller instanceof Function ?
                            this.controller : this.controller.slice(-1)[0];
                        $injector.invoke(this.controller, thisController, {
                            '$scope': this.scope,
                            '$modal': {
                                $$thisModal: this,
                                hide: (value) => {
                                    this.close().then(() => {
                                        this.defer.resolve(value);
                                    });
                                },
                                close: (value) => {
                                    this.close().then(() =>{
                                        this.defer.reject(value);
                                    });
                                }
                            }
                        });
                    }
                    this.showBackdrop();
                    this.$element = $compile(template)(this.scope);
                    angular.element(document.body)
                        .append(this.$element);
                    setTimeout(() => $(this.$element).find('[autofocus]').focus(), 0);
                });
                return this;
            }

            close() {
                return $q((resolve) => {
                    this.showBackdrop(true);
                    angular.element(this.$element).remove();
                    this.scope.$destroy();
                    resolve();
                });
            }

            showBackdrop (isRemove) {
                if (isRemove) {
                    angular.element(document.querySelector('div.overlay')).remove();
                } else if (this.backdrop) {
                    var backdropElement = angular.element('<div class="overlay"> </div>').css({
                        'opacity': this.backdrop.opacity !== undefined ? this.backdrop.opacity : 0.6
                    });
                    angular.element(document.body)
                        .append(backdropElement);
                }
            }
        }

        this.show = function (templateUrl, options) {
            return (new Modal(templateUrl, options || {})).show().defer.promise;
        };
    });

    app.directive('modal', function () {
        return {
            restrict: 'E',
            templateUrl: '/components/modal/modal.html',
            transclude: true
        }
    });

}
